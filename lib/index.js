#!/usr/bin/env node
var util = require('util'),
  events = require('events')
var musicmetadata = require('musicmetadata')
var Iconv  = require('iconv').Iconv;

var program = require('commander');
var path = require('path');
var fs = require('fs');

program.version('0.0.1')
.usage('[options] <path>')
  .option('-r, --recursive', 'Recursive')
  .option('-p, --prefix <prefix>', 'Prefix', '')
  .option('-o, --output <output>', 'Output Path', null)
  .option('-f, --format <format>', 'Formats', ['mp3', 'mp4', 'ogg'])
  .parse(process.argv);

var source_path = '.'

if (program.args.length > 0) {
  source_path = program.args[0];
} else {
  source_path = '.';
}

var matcher = new RegExp('\.(' + program.format.join('|') + ')$');
var iconv = new Iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE');

//Find files
// For each file, load metadata,
//

function DirectoryTraverser(source_path, recursive, matcher) {
  var self = this;

  var queued_paths = [ source_path ];
  var current_path = '';
  var current_contents = [];

  function launch() {
    continueFunction()
  }

  function continueFunction() {
    while (current_contents.length > 0 || queued_paths.length > 0) {

      if (current_contents.length > 0 ) {
        var current_file = current_contents.shift();

        var fullpath = path.join(current_path, current_file);
        var fsStat = fs.statSync(fullpath);

        if (fsStat.isDirectory()) {

          if (program.recursive) {
            queued_paths.push(fullpath)
          }

        } else if (matcher.test(current_file)) {
          return self.emit('match', fullpath)
        }
      } else {
        current_path = queued_paths.shift()
        current_contents = fs.readdirSync(current_path);
      }
    }
    self.emit('complete')
  }

  this.launch = launch;
  this.continue = continueFunction;
}

util.inherits(DirectoryTraverser, events.EventEmitter)

var traverser = new DirectoryTraverser(source_path, program.recursive, matcher);

var counter = 0;

traverser.on('match', function(match) {
  var yes = false;
  var stream = fs.createReadStream(match);
  var parser = musicmetadata(stream);

  parser.on("metadata", function(tags) {
    yes = true;
    counter += 1;

    var file = program.prefix + match
    var title = ''

    var albumartist = tags.albumartist.join(', ')
    var artist = tags.artist.join(', ')

    if (tags.disk && tags.disk.no > 0)
      title += tags.disk.no + '.'

    if (tags.track && tags.track.no > 0)
      title += (tags.track.no < 10 ? '0' : '') + tags.track.no  + ' '

    if (albumartist)
      title += iconv.convert(albumartist) + ' - '

    if (tags.album)
      title += iconv.convert(tags.album) + ' - '

    if (artist && (artist != albumartist))
      title += '(' + iconv.convert(artist) + ') '

    title += iconv.convert(tags.title ? tags.title : path.basename(match))

    output_stream.write('File' + counter + '=' + file + '\n')

    output_stream.write('Title' + counter + '=' + title + '\n')

    output_stream.write('Length' + counter + '=' + tags.duration + '\n')
  });

  parser.on("done", function(){
    if (! yes){
      counter += 1;
      output_stream.write('File' + counter + '=' + program.prefix + match + '\n')
      console.warn("No metadata for: " + match)
    }

    stream.destroy();
    traverser.continue();
  })
})

traverser.on('complete', function() {
  output_stream.write('NumberOfEntries=' + counter + '\nVersion=2\n')

  if (output_stream != process.stdout)
  {
    output_stream.on('drain', function() {
      output_stream.end()
    })
  } else {

    process.exit()
  }
})

var output_stream;

if (program.output) {
  output_stream = fs.createWriteStream(program.output, {flags: 'w', encoding: 'ISO-8859'})
  output_stream.on('open', begin);
} else {
  output_stream = process.stdout
  begin()
}

function begin() {
  output_stream.write('[playlist]\n')
  traverser.launch();
}



